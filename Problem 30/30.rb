def find_numbers(power)
  result = []
  i = 2
  while i < 1_000_000 do
    result << i if i.digits.map { |digit| digit ** power }.inject(:+) == i
    i += 1
  end

  result
end

puts find_numbers(5).inject(:+)
