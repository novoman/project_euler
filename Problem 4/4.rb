def is_palindrome?(number)
  str = number.to_s

  str == str.reverse
end

def max_palindrome
  i = 999_999
  while i >= 10_001 do
    if is_palindrome?(i)
      a = 999
      while a >= 100 do
        return i if i % a == 0 && (i / a).digits.count == 3
        a -= 1
      end
    end
    i -= 1
  end
end

puts max_palindrome
