NUMBER_BEFORE = 2_000_000.freeze

def find_sum(number)
  arr = {}
  i = 2

  while i <= number do
    arr[i] = true
    i += 1
  end

  i = 2
  while i * i <= number
    if arr[i]
      j = i * i
      while j <= number
        arr[j] = false
        j += i
      end
    end
    i += 1
  end

  arr.select{ |_, el| el }.keys.inject(:+)
end

puts find_sum(NUMBER_BEFORE)
