def find_sum(side_length)
  sum = 1
  i = 1
  j = 2
  while j <= side_length do
    4.times do
      i += j
      sum += i
    end
    j += 2
  end

  sum
end

puts find_sum(1001)
