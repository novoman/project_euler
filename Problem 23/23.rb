require "prime"

LIMIT = 28123

class Integer
  def proper_divisors
    return [] if self == 1
    primes = prime_division.flat_map{|prime, freq| [prime] * freq}
    (1...primes.size).each_with_object([1]) do |n, res|
      primes.combination(n).map{|combi| res << combi.inject(:*)}
    end.flatten.uniq
  end

  def sum_of_divisors
    self.proper_divisors.inject(:+)
  end
end

def abundant_numbers
  result = []

  (2..LIMIT).each do |number|
    result << number if number.sum_of_divisors > number
  end

  result
end

def array_of_pair_sums
  arr_of_sum = []
  numbers = abundant_numbers

  i = 0
  while i < numbers.count do
    j = i
    while j < numbers.count do
      arr_of_sum << numbers[i] + numbers[j]
      j += 1
    end
    i += 1
  end

  arr_of_sum.uniq.sort
end

def find_sum
  sum = 0
  sums_arr = array_of_pair_sums

  (1..LIMIT).each do |number|
    sum += number unless sums_arr.include?(number)
  end

  sum
end

puts find_sum
