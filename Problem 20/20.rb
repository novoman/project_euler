def sum_of_digits(number)
  number.to_s.chars.map(&:to_i).inject(:+)
end

def factorial(number)
  (2..number).inject(:*)
end

puts sum_of_digits(factorial(100))
