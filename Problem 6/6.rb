# sum(n) = n(2n + 1)(n + 1)/6
def sum_of_squares(number)
  number * (2 * number + 1) * (number + 1) / 6
end

# sum(n) = n(n + 1)/2
def square_of_sum(number)
  sum = number * (number + 1) /2

  sum ** 2
end

puts square_of_sum(100) - sum_of_squares(100)
