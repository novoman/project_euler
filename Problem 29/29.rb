def array_of_powers(max_pow)
  result = []
  a = 2
  while a <= max_pow do
    b = 2
    while b <= max_pow do
      result << a ** b
      b += 1
    end
    a +=1
  end

  result.uniq
end

p array_of_powers(100).count
