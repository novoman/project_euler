PRIME_NUMBERS_BEFORE = 141_000

def arr_primes(number)
  arr = {}
  i = 2

  while i <= PRIME_NUMBERS_BEFORE do
    arr[i] = true
    i += 1
  end

  i = 2
  while i * i <= PRIME_NUMBERS_BEFORE
    if arr[i]
      j = i * i
      while j <= PRIME_NUMBERS_BEFORE
        arr[j] = false
        j += i
      end
    end
    i += 1
  end

  arr.select{ |_, el| el }.keys.first(number).last
end

puts arr_primes(10_001)
