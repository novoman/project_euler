require 'time'

def find_sundays_on_the_first
  t1 = DateTime.new(1901,1,1)
  t2 = DateTime.new(2000,12,31)
  date_range = t1..t2

  date_range.select(&:sunday?).select { |date| date.day == 1 }.count
end

puts find_sundays_on_the_first
