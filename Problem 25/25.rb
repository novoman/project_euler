def find_index_of_number_which_contains_digits(count)
  fibonacci_arr = []
  fibonacci_arr[0] = 1
  fibonacci_arr[1] = 1

  i = 2
  while true do
    fibonacci_arr[i] = fibonacci_arr[i - 1] + fibonacci_arr[i - 2]
    return i + 1 if fibonacci_arr[i].digits.count == count
    i += 1
  end
end

puts find_index_of_number_which_contains_digits(1_000)
