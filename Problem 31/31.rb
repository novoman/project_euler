CURRENCY = [1, 2, 5, 10, 20, 50, 100, 200]

def count_ways(n)
  table = Array.new(n + 1, 0)
  table[0] = 1
  i = 0
  while i < CURRENCY.count do
    j = CURRENCY[i]
    while j <= n do
      table[j] += table[j - CURRENCY[i]]
      j += 1
    end
    i += 1
  end

  table[n]
end

puts count_ways(200)
