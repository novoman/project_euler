require 'prime'

def find_max_n(a, b)
  n = 0
  while true do
    result = n ** 2 + a * n + b
    break unless Prime.prime?(result)
    n += 1
  end

  n
end

def find_coefficients_product
  a = -999
  max_n = 0
  coefficients = []

  while a < 1_000 do
    b = -1_000
    while b <= 1_000 do
      temp_n = find_max_n(a, b)
      if temp_n > max_n
        max_n = temp_n
        coefficients[0] = a
        coefficients[1] = b
      end
      b += 1
    end
    a += 1
  end

  coefficients[0] * coefficients[1]
end

puts find_coefficients_product
