def get_sum_divisible_by(number, max)
  p = max / number

  number * (p * (p + 1)) / 2
end

puts get_sum_divisible_by(3, 999) + get_sum_divisible_by(5, 999) - get_sum_divisible_by(15, 999)
