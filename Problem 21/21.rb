require "prime"

class Integer
  def proper_divisors
    return [] if self == 1
    primes = prime_division.flat_map{|prime, freq| [prime] * freq}
    (1...primes.size).each_with_object([1]) do |n, res|
      primes.combination(n).map{|combi| res << combi.inject(:*)}
    end.flatten.uniq
  end

  def sum_of_divisors
    self.proper_divisors.inject(:+)
  end
end

def sum_of_amicable_numbers
  result = 0

  (2...10_000).each do |number|
    sum = number.sum_of_divisors
    result += sum + number if number == sum.sum_of_divisors && number != sum
  end

  result / 2
end

puts sum_of_amicable_numbers
