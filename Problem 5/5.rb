def prime_dividers(number)
  arr = []
  i = 2
  while number > 1 do
    if number % i == 0
      number /= i
      arr << i
    else
      i += 1
    end
  end

  arr
end

def lcm(number)
  arr = []
  i = 2
  while i <= number do
    prime_arr = prime_dividers(i)

    prime_arr.each do |el|
      arr.delete_at(arr.index(el)) if arr.index(el)
    end

    arr += prime_arr
    i += 1
  end

  arr.inject(:*)
end

puts lcm(20)
