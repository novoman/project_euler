SIZE = 20

class Integer
  def fact
    (1..self).reduce(:*) || 1
  end
end

# n!/k!*(n-k)!
def find_routes_count
  n = SIZE * 2
  k = SIZE

  n.fact / (k.fact * (n-k).fact)
end

puts find_routes_count
