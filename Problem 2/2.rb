def get_sum
  sum = 0
  last_number_1 = 1
  last_number_2 = 1
  number = last_number_1 + last_number_2

  while number < 4_000_000
    sum += number

    last_number_1 = number + last_number_2
    last_number_2 = number + last_number_1
    number = last_number_1 + last_number_2
  end

  sum
end

puts get_sum
