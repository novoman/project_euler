def chain_count(number)
  count = 1
  while number > 1 do
    number = number % 2 == 0 ? number / 2 : 3 * number + 1
    count += 1
  end

  count
end

def find_max_chain
  max_number = 0
  max_count = 0
  (500_000..1_000_000).each do |number|
    current_count = chain_count(number)
    if current_count > max_count
      max_count = current_count
      max_number = number
    end
  end

  max_number
end

puts find_max_chain
