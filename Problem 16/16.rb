def sum_of_digits(number)
  number.to_s.chars.map(&:to_i).inject(:+)
end

puts sum_of_digits(2 ** 1_000)
