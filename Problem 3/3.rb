def lpf(number)
  arr = arr_primes(Math.sqrt(number))

  arr.each do |prime|
    return prime if (number % prime == 0)
  end
end

def arr_primes(number)
  arr = {}
  i = 2

  while i <= number do
    arr[i] = true
    i += 1
  end

  i = 2
  while i * i <= number
    if arr[i]
      j = i * i
      while j <= number
        arr[j] = false
        j += i
      end
    end
    i += 1
  end

  arr.select{ |_, el| el }.keys.reverse
end

puts lpf(600851475143)
