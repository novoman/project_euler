LETTERS_COUNT_FOR_NUMBERS = {
  1 => 'one'.chars.count,
  2 => 'two'.chars.count,
  3 => 'three'.chars.count,
  4 => 'four'.chars.count,
  5 => 'five'.chars.count,
  6 => 'six'.chars.count,
  7 => 'seven'.chars.count,
  8 => 'eight'.chars.count,
  9 => 'nine'.chars.count,
  10 => 'ten'.chars.count,
  11 => 'eleven'.chars.count,
  12 => 'twelve'.chars.count,
  13 => 'thirteen'.chars.count,
  14 => 'fourteen'.chars.count,
  15 => 'fifteen'.chars.count,
  16 => 'sixteen'.chars.count,
  17 => 'seventeen'.chars.count,
  18 => 'eighteen'.chars.count,
  19 => 'nineteen'.chars.count,
  20 => 'twenty'.chars.count,
  30 => 'thirty'.chars.count,
  40 => 'forty'.chars.count,
  50 => 'fifty'.chars.count,
  60 => 'sixty'.chars.count,
  70 => 'seventy'.chars.count,
  80 => 'eighty'.chars.count,
  90 => 'ninety'.chars.count,
  100 => 'hundred'.chars.count,
  1_000 => 'onethousand'.chars.count
}

def letters_count(number)
  temp_sum = 0

  return LETTERS_COUNT_FOR_NUMBERS[number] if number == 1_000

  if number / 100 >= 1
    temp_sum += LETTERS_COUNT_FOR_NUMBERS[number / 100] + LETTERS_COUNT_FOR_NUMBERS[100]
    number %= 100
    temp_sum += 'and'.chars.count if number > 0
  end

  if number / 10 >= 2
    temp_sum += LETTERS_COUNT_FOR_NUMBERS[number / 10 * 10]
    number %= 10
  end

  temp_sum += LETTERS_COUNT_FOR_NUMBERS[number] if LETTERS_COUNT_FOR_NUMBERS.key?(number)

  temp_sum
end

def find_sum(num)
  sum = 0
  (1..num).each { |number| sum += letters_count(number) }

  sum
end

puts find_sum(1_000)
