SUM = 1_000.freeze

def find_product
  a = 1
  while a < SUM do
    b = a + 1
    while b < SUM do
      c = Math.sqrt((a ** 2 + b ** 2))
      if (c % 1).zero?
        c = c.to_i
        return a * b * c if a + b + c == SUM
      end
      b += 1
    end
    a += 1
  end
end

puts find_product
