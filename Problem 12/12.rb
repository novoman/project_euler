require "prime"

class Integer
  def proper_divisors
    return [] if self == 1
    primes = prime_division.flat_map{|prime, freq| [prime] * freq}
    (1...primes.size).each_with_object([1]) do |n, res|
      primes.combination(n).map{|combi| res << combi.inject(:*)}
    end.flatten.uniq
  end
end


def triangle_numbers(i)
  sum = i * (i + 1) / 2
  while true do
    return sum if sum.proper_divisors.size > 500

    i += 1
    sum += i
  end
end

puts triangle_numbers(3000)
